// webpack.mix.js

let mix = require('laravel-mix');

mix.webpackConfig({
    resolve: {
        modules: [
            'node_modules'
        ],
        alias: {
            jquery: 'jquery/src/jquery'
        }
    }
});

mix.autoload({
    jquery: ['$', 'window.jQuery', 'jQuery'],
    aos : ['AOS'],
});

// end fix


mix.js('src/js/app.js', 'dist').setPublicPath('public');
mix.sass('src/sass/app.scss', 'dist').setPublicPath('public');
mix.copyDirectory('src/img', 'public/images');