var startDelay = 500;

// ======================================================
// Animation helper functions
// ======================================================

var animate = function(element, delay = 0, enter = true){
    if(enter) {
        setTimeout(() => { $(element).addClass('aos-animate'); }, delay);
    } else {
        setTimeout(() => { $(element).removeClass('aos-animate'); }, delay);
    }
}

var activate = function(element, delay = 0, enter = true){
    if(enter) {
        setTimeout(() => { $(element).addClass('active'); }, delay);
    } else {
        setTimeout(() => { $(element).removeClass('active'); }, delay);
    }
}

// ======================================================
// This code will execute in all pages
// ======================================================

animate("#footer .aos-init", startDelay);

// ======================================================
// This code will execute in index.html
// ======================================================

if($('body').hasClass('step-1')){

        // initial status animation 
        animate("#init-card-wrapper", startDelay + 500);
        animate("#init-box-wrapper", startDelay + 1000);
        
        // looping animations
        setInterval(() => {
            $(".smile-cta").toggleClass('animate__animated  animate__tada');
        }, 1500);
        
        setInterval(() => {
            $("#cta-unwarp-gifts").toggleClass('animate__animated  animate__pulse animate__faster');
        }, 2000);
        
        
        // initial stage
        animate("#stage-1", startDelay + 2000);
        activate("#stage-1", startDelay + 2000);
        
        // actions from stage 1 to 2
        $("#stage-1").on('click', function(){
            animate("#stage-1", 0, false);
            activate("#stage-1", 0, false);
        
            activate("#stage-2", 0);
            animate("#stage-2", 0);
        });
        
        // actions from stage 2 to 1 (read again message)
        $('a.to-stage').on('click', function(){
            animate("#stage-2", 0, false);
            activate("#stage-2", 0, false);
        
            animate("#stage-1", 0);
            activate("#stage-1", 0);
        });
        
        $("#cta-unwarp-gifts").on('click', function(){
            activate(".stage.active", 0, false);
        
            activate("#stage-3", 0);
            animate("#stage-3", 0); 
        });
        
        $("#stage-3 .close").on('click', function(){
            activate(".stage.active", 0, false);
            animate("#stage-3", 0, false); 
        
            activate("#stage-1", 0);
        });

}

// ======================================================
// This code will execute in index-2.html
// ======================================================
if($('body').hasClass('step-2')){

    video = document.querySelector('video');
    video.play();

    var startDelay = 11000;

    animate("#stage-4", 250);
    activate("#stage-4", 250);

    setTimeout(() => {  $('#gifts-w-info').addClass('aos-animate');  }, startDelay);
    setTimeout(() => {  $('#pre-card').addClass('aos-animate');  }, startDelay);

    setTimeout(() => {  $("#stage-click").addClass('clickable');  }, startDelay);

    $("#stage-click").on('click', function(){
        if($(this).hasClass('clickable')) {
            $(this).removeClass('front');
            $("#stage-click").removeClass('clickable');
            
            $('#gifts-holder').removeClass('pre');
            $('#stage-animation').removeClass('pre');

            setTimeout(() => {  $('#gifts-holder').removeClass('stacked');  }, 500);
            
            // drops the package
            setTimeout(() => {  $(".gift-wrapper.first").addClass('droped'); }, 1000);
            setTimeout(() => {  $(".gift-wrapper.first .inner-gift-wrapper").removeClass('aos-animate'); }, 1000);
            
            setTimeout(() => {  $('.tabbed-menu-1').addClass('aos-animate');  }, 1000);
            
            setTimeout(() => {  $('#feedback-btn').addClass('aos-animate');  }, 1000);
            setTimeout(() => {  $('#feedback-btn-cta').addClass('aos-animate');  }, 1500);
            
            setTimeout(() => {  $('#feedback-btn-m').addClass('aos-animate');  }, 1000);
            setTimeout(() => {  $('#feedback-btn-mcta').addClass('aos-animate');  }, 1500);
            setTimeout(() => {  $('#back-to-start').addClass('active');  }, 2000);

        }
    });


    $(".gift-wrapper:not(.first)").on('click', function(){
        $(".gift-wrapper").not($(this)).removeClass('active');
        setTimeout(() => {  $('#gifts-holder').addClass('active');  }, 0);
        $this = $(this);
        setTimeout(() => {
            $this.addClass('showing');
            $('#gifts-holder').addClass('showing'); 
            $("#gift-info").addClass('active');
            $(".tabbed-menu-1").removeClass('aos-animate');
            $(".tabbed-menu-2").addClass('aos-animate');

        }, 250);

        setTimeout(() => {  $('body').addClass('scroll');  }, 500);

        setTimeout(() => {  $('#feedback-btn').removeClass('aos-animate');  }, 750);
        setTimeout(() => {  $('#feedback-btn-cta').removeClass('aos-animate');  }, 500);

        setTimeout(() => {  $('#feedback-btn-m').removeClass('aos-animate');  }, 750);
        setTimeout(() => {  $('#feedback-btn-mcta').removeClass('aos-animate');  }, 500);

        $(this).addClass('active');
    });

    $("#back-to-stack").on('click', function(e){
        e.preventDefault();
        $(".gift-wrapper").removeClass('active showing');
        $('#gifts-holder').removeClass('active showing');
        $("#gift-info").removeClass('active');
        $(".tabbed-menu-2").removeClass('aos-animate');
        $(".tabbed-menu-1").addClass('aos-animate');

        setTimeout(() => {  $('body').removeClass('scroll');  }, 10);


        setTimeout(() => {  $('#feedback-btn').addClass('aos-animate');  }, 500);
        setTimeout(() => {  $('#feedback-btn-cta').addClass('aos-animate');  }, 750);

        setTimeout(() => {  $('#feedback-btn-m').addClass('aos-animate');  }, 500);
        setTimeout(() => {  $('#feedback-btn-mcta').addClass('aos-animate');  }, 750);

        $('html,body').animate({ scrollTop: 0 }, 'slow');

    });

    $("#go-to-card").on('click', function(e){
        e.preventDefault();

        setTimeout(() => {  $('#gifts-holder').addClass('stacked');  }, 250);
        setTimeout(() => {  $('#gifts-w-info').removeClass('aos-animate');  }, 500);

        animate("#stage-5", 750);
        activate("#stage-5", 750);

        animate("#stage-4", 750, false);
        activate("#stage-4", 750, false);

        $("#go-to-gifts").removeClass('active');
        $(this).addClass('active');
    });

    $("#go-to-gifts").on('click', function(e){
        e.preventDefault();

        animate("#stage-5", 250, false);
        activate("#stage-5", 250, false);

        animate("#stage-4", 250);
        activate("#stage-4", 250);

        setTimeout(() => {  $('#gifts-w-info').addClass('aos-animate');  }, 500);
        setTimeout(() => {  $('#gifts-holder').removeClass('stacked');  }, 750);

        $(this).addClass('active');
        $("#go-to-card").removeClass('active');
    });

    $("#feedback-btn-mcta").on('click', function(e){
        if(!$(this).hasClass('active')) {
            $(this).addClass('active');
            $(this).focus();
            e.preventDefault();
        } else {
            return true;
        }
    });

    $("#feedback-btn-mcta").on('focusout', function(e){
        $(this).removeClass('active');
    });


    $("#inner-card-wrapper").on('mousemove', function(e){
        offsetTop = $(this).offset().top;
        offsetLeft = $(this).offset().left;

        $('.fcta').css({
            top: e.pageY - offsetTop,
            left: e.pageX - offsetLeft
        });
    });
    $("#inner-card-wrapper").on('mouseenter', function(){
        $('.fcta').removeClass('out');
    });
    $("#inner-card-wrapper").on('mouseleave', function(){
        // $('.fcta').css({
        //     top: '',
        //     left: ''
        // });
        $('.fcta').addClass('out');
    });

}
